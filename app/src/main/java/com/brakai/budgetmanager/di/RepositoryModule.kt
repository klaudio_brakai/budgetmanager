package com.brakai.budgetmanager.di

import com.brakai.budgetmanager.data.repository.AccountsRepositoryImpl
import com.brakai.budgetmanager.domain.repository.AccountsRepository
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class RepositoryModule {

    @Binds
    @Singleton
    abstract fun provideAccountRepository(accountsRepositoryImpl: AccountsRepositoryImpl): AccountsRepository

}