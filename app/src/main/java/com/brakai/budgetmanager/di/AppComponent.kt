package com.brakai.budgetmanager.di

import android.content.Context
import com.brakai.budgetmanager.presentation.utils.ViewModelModule
import com.brakai.budgetmanager.presentation.view.account.AccountCreation
import com.brakai.budgetmanager.presentation.view.account.AccountsFragment
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = [ViewModelModule::class, RepositoryModule::class])
interface AppComponent {
    fun inject(fragment: AccountCreation)
    fun inject(fragment: AccountsFragment)

    @Component.Factory
    interface Factory {
        fun create(
            @BindsInstance context: Context,
            @BindsInstance auth: FirebaseAuth,
            @BindsInstance firestore: FirebaseFirestore
        ): AppComponent
    }

}