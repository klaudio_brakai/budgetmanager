package com.brakai.budgetmanager.data.repository

import com.brakai.budgetmanager.domain.model.Account
import com.brakai.budgetmanager.domain.repository.AccountsRepository
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.toObjects
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.tasks.await
import timber.log.Timber
import javax.inject.Inject

class AccountsRepositoryImpl @Inject constructor(
    private val firebaseAuth: FirebaseAuth,
    private val firestore: FirebaseFirestore
) :
    AccountsRepository {


    override suspend fun createAccount(account: Account): Boolean {
        val userID = firebaseAuth.uid ?: throw Throwable("Not logged in")
        val accountRef = firestore.collection("users").document(userID).collection("accounts")
            .document(account.id)
        return try {
            accountRef.set(account).await()
            Timber.tag("firebase").d("account ${accountRef.id} created")
            true
        } catch (e: Throwable) {
            Timber.tag("firebase").e(e, "account creation failed")
            false
        }
    }

    override fun loadAccounts(): Flow<List<Account>> {
        val userID = firebaseAuth.uid ?: throw Throwable("Not logged in")
        return flow {
            val querySnapshot =
                firestore.collection("users").document(userID).collection("accounts").get().await()
            val accounts = querySnapshot.toObjects<Account>()
            emit(accounts)
        }
    }
}