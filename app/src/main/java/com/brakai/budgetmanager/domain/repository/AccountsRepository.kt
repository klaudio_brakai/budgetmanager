package com.brakai.budgetmanager.domain.repository

import com.brakai.budgetmanager.domain.model.Account
import kotlinx.coroutines.flow.Flow

interface AccountsRepository {

    suspend fun createAccount(account: Account): Boolean

    fun loadAccounts(): Flow<List<Account>>

}