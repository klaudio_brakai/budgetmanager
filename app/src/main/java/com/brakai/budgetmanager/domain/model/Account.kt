package com.brakai.budgetmanager.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import kotlinx.serialization.Serializable
import java.util.*

@Serializable
@Parcelize
data class Account(
    val id: String,
    var name: String,
    var description: String,
    var balance: String,
    var type: String
) : Parcelable {
    constructor() : this(UUID.randomUUID().toString(), "", "", "", "")
}