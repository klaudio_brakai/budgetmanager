package com.brakai.budgetmanager.presentation.view.account

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.brakai.budgetmanager.databinding.AccountsLayoutBinding
import com.brakai.budgetmanager.presentation.MyApplication
import com.brakai.budgetmanager.presentation.viewmodel.AccountsViewModel
import kotlinx.android.synthetic.main.accounts_layout.*
import javax.inject.Inject

class AccountsFragment : Fragment() {

    private lateinit var binding: AccountsLayoutBinding

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    val viewModel: AccountsViewModel by lazy {
        ViewModelProvider(this, viewModelFactory).get(AccountsViewModel::class.java)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        MyApplication.instance.appComponent.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = AccountsLayoutBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val pagerAdapter = AccountAdapter(requireActivity(), listOf())
        accountsViewPager.adapter = pagerAdapter
        accountsViewPager.setPageTransformer(DepthPageTransformer())

        viewModel.loadAccounts().observe(viewLifecycleOwner, Observer { acs ->
            if (!acs.isNullOrEmpty()) {
                pagerAdapter.accounts = acs
                pagerAdapter.notifyDataSetChanged()
            }
        })

    }
}