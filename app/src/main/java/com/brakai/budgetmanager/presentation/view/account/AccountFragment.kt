package com.brakai.budgetmanager.presentation.view.account

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.brakai.budgetmanager.databinding.AccountCardItemLayoutBinding
import com.brakai.budgetmanager.domain.model.Account
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.components.Description
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry


class AccountFragment : Fragment() {

    companion object {
        private const val ACCOUNT_ARG = "account"

        @JvmStatic
        fun newInstance(account: Account) = AccountFragment().apply {
            arguments = Bundle().apply {
                putParcelable(ACCOUNT_ARG, account)
            }
        }
    }

    private lateinit var binding: AccountCardItemLayoutBinding
    private var account: Account? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = AccountCardItemLayoutBinding.inflate(inflater, container, false)
        binding.item = account
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        account = arguments?.getParcelable<Account>(ACCOUNT_ARG)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.accountPieChart.let { pieChart: PieChart ->
            val entries: MutableList<PieEntry> = ArrayList()
            val set = PieDataSet(entries, "No data")
            set.color = Color.GRAY
            val data = PieData(set)
            pieChart.data = data
            pieChart.setUsePercentValues(true)
            pieChart.description = Description().apply { this.text = "Categories" }
            pieChart.invalidate()
        }
    }
}