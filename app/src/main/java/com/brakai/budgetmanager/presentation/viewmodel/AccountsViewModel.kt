package com.brakai.budgetmanager.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.brakai.budgetmanager.domain.model.Account
import com.brakai.budgetmanager.domain.repository.AccountsRepository
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

class AccountsViewModel @Inject constructor(private val accountsRepository: AccountsRepository) :
    ViewModel() {

    private lateinit var accounts: MutableLiveData<List<Account>>

    fun loadAccounts(): LiveData<List<Account>> {
        if (!::accounts.isInitialized) {
            accounts = MutableLiveData()
            viewModelScope.launch {
                accountsRepository.loadAccounts().collect {
                    accounts.postValue(it)
                }
            }
        }
        return accounts
    }


}