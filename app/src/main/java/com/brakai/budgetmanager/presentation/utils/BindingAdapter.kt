package com.brakai.budgetmanager.presentation.utils

import androidx.databinding.BindingAdapter
import com.google.android.material.textfield.TextInputLayout

object BindingAdapter {

    @BindingAdapter("errorText")
    @JvmStatic
    fun TextInputLayout.errorText(text: String?) {
        error = text
    }
}