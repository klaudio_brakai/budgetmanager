package com.brakai.budgetmanager.presentation.view.account

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.core.widget.doAfterTextChanged
import androidx.lifecycle.ViewModelProvider
import com.brakai.budgetmanager.R
import com.brakai.budgetmanager.databinding.AccountCreationFragmentBinding
import com.brakai.budgetmanager.domain.model.Event
import com.brakai.budgetmanager.domain.model.EventObserver
import com.brakai.budgetmanager.presentation.MyApplication
import com.brakai.budgetmanager.presentation.utils.showSnackbar
import com.brakai.budgetmanager.presentation.viewmodel.AccountCreationViewModel
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textview.MaterialTextView
import java.math.BigDecimal
import javax.inject.Inject

class AccountCreation : BottomSheetDialogFragment() {

    companion object {
        fun newInstance() = AccountCreation()
    }

    private lateinit var binding: AccountCreationFragmentBinding

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    val viewModel: AccountCreationViewModel by lazy {
        ViewModelProvider(this, viewModelFactory).get(AccountCreationViewModel::class.java)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        MyApplication.instance.appComponent.inject(this)
    }


    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        binding = AccountCreationFragmentBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        return binding.root
    }

    @Suppress("DEPRECATION")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        binding.accountBalance.setEndIconOnClickListener {
            negateBalance()
        }
        binding.accountBalance.editText?.doAfterTextChanged { balance ->
            if (balance.isNullOrBlank() || balance.toString() == "-") return@doAfterTextChanged
            val currentBalance = BigDecimal(balance.toString())
            if (currentBalance >= BigDecimal.ZERO)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    binding.accountBalance.editText?.setTextColor(
                        resources.getColor(
                            R.color.positive_currency,
                            null
                        )
                    )
                } else {
                    binding.accountBalance.editText?.setTextColor(resources.getColor(R.color.positive_currency))
                }
            else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    binding.accountBalance.editText?.setTextColor(
                        resources.getColor(
                            R.color.negative_currency,
                            null
                        )
                    )
                } else {
                    binding.accountBalance.editText?.setTextColor(resources.getColor(R.color.negative_currency))
                }
            }
        }

        viewModel.status.observe(viewLifecycleOwner, EventObserver {
            when (it) {
                AccountCreationViewModel.Status.Idle -> {
                    binding.continueButton.isEnabled = true
                }
                AccountCreationViewModel.Status.Loading -> {
                    binding.continueButton.isEnabled = false
                }
                is AccountCreationViewModel.Status.Canceled -> {
                    view.showSnackbar("Canceled", Snackbar.LENGTH_LONG)
                    viewModel.status.value = Event(AccountCreationViewModel.Status.Idle)
                    this.dismiss()
                }
                is AccountCreationViewModel.Status.Completed -> {
                    view.showSnackbar("Account created", Snackbar.LENGTH_LONG)
                    viewModel.status.value = Event(AccountCreationViewModel.Status.Idle)
                    this.dismiss()
                }
                is AccountCreationViewModel.Status.Failed -> {
                    view.showSnackbar("Failed", Snackbar.LENGTH_LONG)
                    viewModel.status.value = Event(AccountCreationViewModel.Status.Idle)
                }
            }
        })

        binding.typeSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                viewModel.type.value = "Debit"
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                viewModel.type.value = ((view as? MaterialTextView)?.text ?: "Debit").toString()
            }

        }
    }

    fun negateBalance() {
        binding.accountBalance.editText?.let { editText ->
            if (editText.text.isNullOrBlank() || editText.text.toString() == "-") return@let
            val currentBalance = BigDecimal(editText.text.toString())
            editText.setText(currentBalance.negate().toEngineeringString())
        }
    }

}