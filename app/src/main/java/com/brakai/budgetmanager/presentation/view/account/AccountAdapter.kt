package com.brakai.budgetmanager.presentation.view.account

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.brakai.budgetmanager.domain.model.Account

class AccountAdapter(fa: FragmentActivity, var accounts: List<Account>) : FragmentStateAdapter(fa) {
    override fun getItemCount(): Int = accounts.size

    override fun createFragment(position: Int): Fragment =
        AccountFragment.newInstance(accounts[position])
}