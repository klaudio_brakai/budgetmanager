package com.brakai.budgetmanager.presentation

import androidx.multidex.MultiDexApplication
import com.brakai.budgetmanager.BuildConfig
import com.brakai.budgetmanager.di.DaggerAppComponent
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings
import timber.log.Timber

class MyApplication : MultiDexApplication() {

    companion object {
        lateinit var instance: MyApplication private set
    }

    val appComponent by lazy {
        DaggerAppComponent.factory()
            .create(this, FirebaseAuth.getInstance(), FirebaseFirestore.getInstance())
    }

    override fun onCreate() {
        super.onCreate()

        instance = this

        val settings = FirebaseFirestoreSettings.Builder()
            .setPersistenceEnabled(true)
            .build()
        FirebaseFirestore.getInstance().firestoreSettings = settings

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
            FirebaseFirestore.setLoggingEnabled(true)
        } else {
            FirebaseFirestore.setLoggingEnabled(false)
        }
    }
}