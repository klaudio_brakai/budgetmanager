package com.brakai.budgetmanager.presentation.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.brakai.budgetmanager.R
import com.brakai.budgetmanager.domain.model.Account
import com.brakai.budgetmanager.domain.model.Event
import com.brakai.budgetmanager.domain.repository.AccountsRepository
import com.brakai.budgetmanager.presentation.MyApplication
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*
import javax.inject.Inject

class AccountCreationViewModel @Inject constructor(private val accountsRepository: AccountsRepository) : ViewModel() {

    val name by lazy { MutableLiveData<String>() }
    val nameError by lazy { MutableLiveData<String>() }

    val description by lazy { MutableLiveData<String>() }

    val balance by lazy { MutableLiveData<String>() }

    val status by lazy { MutableLiveData<Event<Status>>() }

    val type by lazy { MutableLiveData<String>() }

    fun submit() {
        if (name.value.isNullOrBlank()) {
            nameError.value =
                MyApplication.instance.applicationContext.getString(R.string.empty_name_error_text)
            return
        }
        status.value = Event(Status.Loading)
        val account = Account(
            id = UUID.randomUUID().toString(),
            name = name.value!!,
            description = description.value ?: "",
            balance = balance.value ?: "0.0",
            type = type.value ?: "Debit"
        )

        createAccountAsync(account)
    }

    private fun createAccountAsync(account: Account) = viewModelScope.launch {
        kotlin.runCatching {
            withContext(Dispatchers.IO) {
                accountsRepository.createAccount(account)
            }
        }.onFailure {
            status.postValue(Event(Status.Failed(it)))
        }.onSuccess { result ->
            if (result) {
                status.postValue(Event(Status.Completed(account.id)))
            } else
                status.postValue(Event(Status.Failed(null)))
        }
    }

    fun cancel() {
        status.postValue(Event(Status.Canceled("Canceled")))
    }

    sealed class Status {
        object Idle : Status()
        object Loading : Status()
        class Canceled(val reason: String) : Status()
        class Completed(val accountID: String) : Status()
        class Failed(val exception: Throwable?) : Status()
    }


}