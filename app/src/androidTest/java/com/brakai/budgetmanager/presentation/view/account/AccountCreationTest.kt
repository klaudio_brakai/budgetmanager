package com.brakai.budgetmanager.presentation.view.account

import android.content.Context
import android.os.Bundle
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.fragment.app.testing.FragmentScenario
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.brakai.budgetmanager.R
import com.brakai.budgetmanager.presentation.viewmodel.AccountCreationViewModel
import com.brakai.budgetmanager.utils.getOrAwaitValue
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class AccountCreationTest {

    private val fragmentArgs = Bundle().apply {}
    private lateinit var instrumentationContext: Context
    private lateinit var scenario: FragmentScenario<AccountCreation>

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        scenario = launchFragmentInContainer<AccountCreation>(fragmentArgs, R.style.AppTheme)
        instrumentationContext = InstrumentationRegistry.getInstrumentation().context
    }

    @Test
    fun testFragmentOpen() {
        assert(true)
    }

    @Test
    fun testWhenEmptyNameThenErrorIsDisplayed() {
        onView(withId(R.id.accountNameET)).perform(typeText(""))
        onView(withId(R.id.continueButton)).perform(click())
        scenario.onFragment {
            assertEquals(
                it.viewModel.nameError.getOrAwaitValue(),
                "Name is required"
            )
        }
    }

    @Test
    fun testTypingOnViews() {
        onView(withId(R.id.accountNameET)).perform(typeText("TEST"))
        onView(withId(R.id.accountNameET)).check(matches(withText("TEST")))

        onView(withId(R.id.accountDescriptionET)).perform(typeText("TEST"))
        onView(withId(R.id.accountDescriptionET)).check(matches(withText("TEST")))

        onView(withId(R.id.accountBalanceET)).perform(typeText("59.69"))
        onView(withId(R.id.accountBalanceET)).check(matches(withText("59.69")))
        scenario.onFragment { it.negateBalance() }
        onView(withId(R.id.accountBalanceET)).check(matches(withText("-59.69")))
    }

    @Test
    fun testSubmit() {
        onView(withId(R.id.accountNameET)).perform(typeText("TEST"))
        onView(withId(R.id.accountNameET)).check(matches(withText("TEST")))

        onView(withId(R.id.accountDescriptionET)).perform(typeText("TEST"))
        onView(withId(R.id.accountDescriptionET)).check(matches(withText("TEST")))

        onView(withId(R.id.accountBalanceET)).perform(typeText("59.69"))
        onView(withId(R.id.accountBalanceET)).check(matches(withText("59.69")))

        onView(withId(R.id.continueButton)).perform(click())
        scenario.onFragment {
            val status = it.viewModel.status.getOrAwaitValue().peekContent()
            assert(status is AccountCreationViewModel.Status.Completed)
        }
    }


}